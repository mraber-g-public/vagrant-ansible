# Vagrant : Ansible #

VM dédiée à l'exécution de Ansible depuis un PC sous Windows...

## Usage ##

* Pour choisir la version de Ansible à utiliser, modifier la variable `ansible_ver` dans le fichier `Vagrantfile` avant de démarrer la VM.
* Déposer les fichiers à utiliser à la racine pour les retrouver dans le répertoire `/vagrant` de la VM

